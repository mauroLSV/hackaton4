## Requirements

- [Python 3.8.x](https://www.python.org/downloads/)
- [Poetry](https://python-poetry.org/docs/#installation)
- [Commitizen](https://github.com/commitizen-tools/commitizen)
- [Git](https://git-scm.com/downloads)

## Installation

After installed `the requirements`, run the following commands:

Global installation and configuration of commitizen

```bash
sudo pip3 install -U Commitizen
```

Run in your terminal

`cz commit` or `cz c`

```bash
git clone git@gitlab.com:you_fork/livex.git
cd livex
```

Run in your terminal to install project's dependencies and config pre-commit hooks

```bash
poetry install
poetry run pre-commit install --hook-type commit-msg
```

To test that it is working, run the following command:

```bash
pre-commit run --all-files
```

The output should be some like as this:

```bash
$ pre-commit run --all-files
[INFO] Initializing environment for https://github.com/pre-commit/pre-commit-hooks.
[INFO] Initializing environment for https://github.com/psf/black.
[INFO] Installing environment for https://github.com/pre-commit/pre-commit-hooks.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
[INFO] Installing environment for https://github.com/psf/black.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
Check Yaml...............................................................Passed
Fix End of Files.........................................................Passed
Trim Trailing Whitespace.................................................Failed
- hook id: trailing-whitespace
- exit code: 1

Files were modified by this hook. Additional output:

Fixing sample.py

black....................................................................Passed
```
