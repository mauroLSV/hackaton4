from django.contrib import admin

from hospital.models import (
    Especialidad,
    Medico,
    Paciente,
    MedicoEspecialidades,
    Horario,
    Citas,
)

# Register your models here.


@admin.register(Medico)
class MedicoAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "nombres",
    )


admin.site.register(Especialidad)


@admin.register(Paciente)
class PacienteAdmin(admin.ModelAdmin):
    list_display = ("nombre",)


admin.site.register(MedicoEspecialidades)


@admin.register(Citas)
class CitasAdmin(admin.ModelAdmin):
    list_display = ("pk",)


admin.site.register(Horario)
