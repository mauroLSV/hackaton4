# Generated by Django 4.0.5 on 2022-06-10 05:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hospital', '0002_alter_citas_activo_alter_especialidad_activo_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='citas',
            name='usuario_registro',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='especialidad',
            name='usuario_registro',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='horario',
            name='usuario_registro',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='medico',
            name='usuario_registro',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='medicoespecialidades',
            name='usuario_registro',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='paciente',
            name='usuario_registro',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
