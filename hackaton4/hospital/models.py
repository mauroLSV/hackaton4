from django.db import models

# Create your models here.


class CommonInfo(models.Model):
    fecha_registro = models.DateField(auto_now=True)
    fecha_modificacion = models.DateField(auto_now=True)
    usuario_registro = models.CharField(max_length=50, blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=100)
    activo = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Medico(CommonInfo):
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    dni = models.IntegerField()
    direccion = models.CharField(max_length=50)
    correo = models.EmailField(max_length=50)
    telefono = models.CharField(max_length=10)
    sexo = models.CharField(max_length=1)
    num_colegiatura = models.CharField(max_length=20)
    fecha_nacimiento = models.DateField()

    def __str__(self):
        return self.nombres

    class Meta:
        unique_together = ["dni"]


class Especialidad(CommonInfo):
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre


class MedicoEspecialidades(CommonInfo):
    medicoid = models.ManyToManyField(Medico)
    especialidad = models.ForeignKey(Especialidad, on_delete=models.CASCADE)


class Paciente(CommonInfo):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    dni = models.CharField(max_length=10)
    direccion = models.CharField(max_length=10)
    telefono = models.CharField(max_length=10)
    sexo = models.CharField(max_length=1)
    fecha_nacimiento = models.DateField()

    def __str__(self):
        return self.nombre


class Citas(CommonInfo):
    ESTADOS_CITAS = (
        ("EN ESPERA", "en espera"),
        ("ATENDIDO", "atendido"),
        ("EN CURSO", "en curso"),
    )
    medicoid = models.ForeignKey(Medico, on_delete=models.CASCADE)
    pacienteid = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    fecha_atencion = models.DateField(auto_now_add=True)
    inicio_atencion = models.DateField(auto_now=True)
    fin_atencion = models.DateField()
    estado = models.CharField(max_length=20, choices=ESTADOS_CITAS)
    observaciones = models.TextField()


class Horario(CommonInfo):
    medico = models.ForeignKey(Medico, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    inicio_atencion = models.DateField()
    fin_atencion = models.DateField()
