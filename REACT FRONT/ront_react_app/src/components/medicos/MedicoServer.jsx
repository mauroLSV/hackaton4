import axios from 'axios'
const API_URL = 'http://localhost:8000/api/medicos/';

export const MedicoList = async () => {
    return await fetch(API_URL);
};

export const registerMedico = async (medico) => {
    axios.post(API_URL, medico, {
        headers: {
            'Content-Type': 'application/json'
        },
    }).then((rest) => { console.log(rest); }).catch((err) => { console.log(err); })
};

export const deleteMedico = async (medico) => {
    console.log(medico)
    axios.delete(`${API_URL}${medico}/`, {
        headers: {
            'Content-Type': 'application/json'
        },
    }).then((rest) => { console.log(rest); }).catch((err) => { console.log(err); })
};

export const updateMedico = async (medicoId, updateMedico) => {

    return axios.put(`${API_URL}${medicoId}/`, updateMedico, {
        headers: {
            'Content-Type': 'application/json'
        },

    });
};

export const getMedico = async (medicoId) => {
    return await fetch(`${API_URL}${medicoId}`);
};