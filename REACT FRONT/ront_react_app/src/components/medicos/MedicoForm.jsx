import * as MedicoServer from './MedicoServer';

import { useEffect, useState } from 'react'
import { useNavigate, useParams } from "react-router-dom";

import { registerMedico } from './MedicoServer'

const MedicoForm = () => {
    const params = useParams();
    // console.log(params)
    let navigate = useNavigate();
    const initialState = {
        fecha_modificacion: "",
        usuario_registro: 1,
        nombre: "",
        usuario_modificacion: "",
        activo: true,
        nombres: "",
        apellidos: "",
        dni: "",
        direccion: "",
        correo: "",
        telefono: "",
        sexo: "",
        num_colegiatura: "",
        fecha_nacimiento: "",

    }

    const [medico, setMedico] = useState(initialState)
    const handleInputChange = (e) => {
        setMedico({ ...medico, [e.target.name]: e.target.value });

    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            if (!params.id) {
                registerMedico(medico).then((rest) => { console.log(rest); }).catch((err) => { console.log(err); });
            } else {
                await MedicoServer.updateMedico(params.id, medico);
            }

            navigate("/medicolist");
        } catch (error) {
            console.log(error);
        }
    };
    const getMedico = async (medicoId) => {
        try {
            const res = await MedicoServer.getMedico(medicoId);
            const data = await res.json();
            const {
                usuario_modificacion,
                activo,
                nombres,
                apellidos,
                dni, direccion,
                correo,
                telefono,
                sexo,
                num_colegiatura,
                fecha_nacimiento
            } = data;
            setMedico({
                usuario_modificacion,
                activo, nombres,
                apellidos, dni,
                direccion, correo,
                telefono, sexo, num_colegiatura,
                fecha_nacimiento
            })

            console.log(data)
        } catch (error) {
            console.log(error)
        }
    }
    useEffect(() => {
        if (params.id) {
            getMedico(params.id);
        }
    }, []);
    return (
        <form onSubmit={handleSubmit}>

            <div className="mb-3">
                <label className="form-label"> usuario modificacion</label>
                <input type="text" className="form-control" name="usuario_modificacion" value={medico.usuario_modificacion} onChange={handleInputChange} />
            </div>
            <div className="mb-3 form-check">
                <input type="checkbox" className="form-check-input" name="activo" value={medico.activo} onChange={handleInputChange} />
                <label className="form-check-label" name="activo" htmlFor="exampleCheck1">Activo</label>
            </div>
            <div className="mb-3">
                <label htmlFor="nombre" className="form-label"> nombre </label>
                <input type="text" className="form-control" name="nombres" value={medico.nombres} onChange={handleInputChange} />
            </div>
            <div className="mb-3">
                <label htmlFor="usuario modificacion" className="form-label">apellidos </label>
                <input type="text" className="form-control" name="apellidos" value={medico.apellidos} onChange={handleInputChange} />
            </div>

            <div className="mb-3">
                <label htmlFor="descripcion" className="form-label">dni</label>
                <input type="text" className="form-control" name="dni" value={medico.dni} onChange={handleInputChange} />
            </div>

            <div className="mb-3">
                <label htmlFor="descripcion" className="form-label">direccion</label>
                <input type="text" className="form-control" name="direccion" value={medico.ddireccionni} onChange={handleInputChange} />
            </div>

            <div className="mb-3">
                <label htmlFor="descripcion" className="form-label">correo</label>
                <input type="text" className="form-control" name="correo" value={medico.correo} onChange={handleInputChange} />
            </div>

            <div className="mb-3">
                <label htmlFor="descripcion" className="form-label">telefono</label>
                <input type="text" className="form-control" name="telefono" value={medico.telefono} onChange={handleInputChange} />
            </div>

            <div className="mb-3">
                <label htmlFor="descripcion" className="form-label">sexo</label>
                <input type="text" className="form-control" name="sexo" value={medico.sexo} onChange={handleInputChange} />
            </div>
            <div className="mb-3">
                <label htmlFor="descripcion" className="form-label">num_colegiatura</label>
                <input type="text" className="form-control" name="num_colegiatura" value={medico.num_colegiatura} onChange={handleInputChange} />
            </div>
            <div className="mb-3">
                <label htmlFor="descripcion" className="form-label">fecha_nacimiento</label>
                <input type="text" className="form-control" name="fecha_nacimiento" value={medico.fecha_nacimiento} onChange={handleInputChange} />
            </div>
            <div >
                {
                    params.id ? (
                        <button type="submit" className="btn btn-primary">editar</button>
                    ) : (
                        <button type="submit" className="btn btn-primary">Submit</button>
                    )
                }

            </div>
        </form>
    )
};

export default MedicoForm;