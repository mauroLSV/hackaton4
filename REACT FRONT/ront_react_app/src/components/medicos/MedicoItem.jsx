import { Link } from 'react-router-dom';
import React from "react";
import {deleteMedico} from './MedicoServer'
import { useNavigate } from "react-router-dom";
const MedicoItem = ({ medic }) => {
    const handleDelete = (medico) => {
        console.log(medico);
        deleteMedico(medico);
    }
    
    const navigate = useNavigate();
    return (
        <div className="col-md-4 mb-4">
            <div className="card card-body">
                <h3 className="card-title">{medic.nombre}</h3>
                <button className={'btn btn-sm btn-info mgs-2'} onClick={() => navigate(`/updatemedico/${medic.id}`)}> Edit</button>
                <p className="card-text">fecha: <strong>{medic.fecha_registro}</strong></p>
                <p className="card-text">fecha: <strong>{medic.fecha_modificacion}</strong></p>
                <p className="card-text">usuario: <strong>{medic.usuario_registro}</strong></p>
                <p className="card-text">usuario: <strong>{medic.nombres}</strong></p>

                <div className="row">
                    <div className="col-3">
                        <Link to="/medicoform" target="_blank" rel="noopener noreferrer" className="btn btn-primary mb-2">
                            crear
                        </Link>
                    </div>
                    <div className="col-3">
                        <form>
                            <button type='submit' onClick={() => medic.id && handleDelete(medic.id)} target="_blank" rel="noopener noreferrer" className="btn btn-danger">
                                eliminar
                            </button>
                        </form>
                    </div>

                </div>

            </div>
        </div>
    );
};
export default MedicoItem;