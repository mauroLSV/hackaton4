import * as MedicoServer from './MedicoServer';

import React, { useEffect, useState } from "react";

//componentes
import MedicoItem from "./MedicoItem";

const MedicoList = () => {

    const [medicos, setMedicos] = useState([]);

    const MedicoList = async () => {
        try {
            const res = await MedicoServer.MedicoList();
            const data = await res.json();
            setMedicos(data)

        } catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        MedicoList();
    }, []);
console.log(medicos)
    return (
        <div className='row'>
            {medicos.map((medic) => { return <MedicoItem key={medic.id} medic={medic} /> }

            )}
        </div>
    )
};
export default MedicoList; 