import { Link } from 'react-router-dom';
const Navbar = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container-fluid">
                <a className="navbar-brand" href="#">CRUD MAURICIO</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link className="nav-link" to="/">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/especialidadList">Especialidades</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/medicolist">Medicos</Link>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Pacientes</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Meidco especialidades</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Citas</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    )
};
export default Navbar;
