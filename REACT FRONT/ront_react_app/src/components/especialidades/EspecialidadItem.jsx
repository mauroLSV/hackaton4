import { Link } from 'react-router-dom';
import React from "react";
import { delteEspecialidades } from './EspecialidadesServer'
import { useNavigate } from "react-router-dom";
const EspecialidadItem = ({ especial }) => {
   const navigate = useNavigate();
    const handleDelete = (especial) => {
        delteEspecialidades(especial)
    }
    return (
        <div className="col-md-4 mb-4">
            <div className="card card-body">
                <h3 className="card-title">{especial.nombre}</h3>
                <button className={'btn btn-sm btn-info mgs-2'}onClick={() => navigate(`/updateEspecialidad/${especial.id}`)}> Edit</button>
                <p className="card-text">Founded: <strong>{especial.fecha_registro}</strong></p>
                <p className="card-text">Founded: <strong>{especial.fecha_modificacion}</strong></p>
                <p className="card-text">Founded: <strong>{especial.usuario_modificacion}</strong></p>
                
                <div className="row">
                    <div className="col-3">
                        <Link to="/especialidadForm" target="_blank" rel="noopener noreferrer" className="btn btn-primary mb-2">
                            crear
                        </Link>
                    </div>
                    <div className="col-3">
                        <form>
                            <button type='submit' onClick={() => especial.id && handleDelete(especial.id)} target="_blank" rel="noopener noreferrer" className="btn btn-danger">
                                eliminar
                            </button>
                        </form>
                    </div>
                   
               </div>
              
            </div>
        </div>
    );
};
export default EspecialidadItem;