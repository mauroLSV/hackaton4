import * as EspecialidadesServer from './EspecialidadesServer';

import React, { useEffect, useState } from "react";

//componentes
import EspecialidadItem from "./EspecialidadItem";

const ListEspecialidades = () => {
    
    const [especialidades, setEspecialidades] = useState([]);

    const listEspecialidades = async () => {
        try {
            const res = await EspecialidadesServer.listEspecialidades();
            const data = await res.json();
            setEspecialidades(data)
            
        } catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        listEspecialidades();
    }, []);

    return (
        <div className='row'>
            {especialidades.map((especial) => (
                <EspecialidadItem key={especial.id} especial={especial} />
                
            ))}
        </div>
    )
};
export default ListEspecialidades;