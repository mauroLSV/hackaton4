import * as EspecialidadesServer from "./EspecialidadesServer";

import { getEspecialidades, registerEspecialidades } from './EspecialidadesServer'
import { useEffect, useState } from 'react'
import { useNavigate, useParams } from "react-router-dom";

const EspecialidadForm = () => {
    const params = useParams();
    console.log(params)
    let navigate = useNavigate();
    const initialState = {
        usuario_registro: "",
        usuario_modificacion: "",
        activo: true,
        nombre: "",
        descripcion: "",

    }

    const [specialidad, setEspecialidad] = useState(initialState)
    const handleInputChange = (e) => {
        setEspecialidad({ ...specialidad, [e.target.name]: e.target.value });

    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            if (!params.id) {
                registerEspecialidades(specialidad).then((rest) => { console.log(rest); }).catch((err) => { console.log(err); });
            } else {
                await EspecialidadesServer.updateEspecialidad(params.id, specialidad);
            }

            navigate("/especialidadList");
        } catch (error) {
            console.log(error);
        }
    };
    const getEspecialidad = async (especialidadId) => {
        try {
            const res = await EspecialidadesServer.getEspecialidades(especialidadId);
            const data = await res.json();
            const { usuario_registro,
                usuario_modificacion,
                activo, nombre, descripcion
            } = data;
            setEspecialidad({
                usuario_registro
                , usuario_modificacion, activo, nombre, descripcion
            })

            console.log(data)
        } catch (error) {
            console.log(error)
        }
    }
    useEffect(() => {
        if (params.id) {
            getEspecialidad(params.id);
        }
    }, []);
    return (
        <form onSubmit={handleSubmit}>

            <div className="mb-3">
                <label className="form-label">usuario registro </label>
                <input type="text" className="form-control" name="usuario_registro" value={specialidad.usuario_registro} onChange={handleInputChange} />
            </div>
            <div className="mb-3">
                <label htmlFor="usuario modificacion" className="form-label">usuario modificacion</label>
                <input type="text" className="form-control" name="usuario_modificacion" value={specialidad.usuario_modificacion} onChange={handleInputChange} />
            </div>
            <div className="mb-3 form-check">
                <input type="checkbox" className="form-check-input" name="activo" value={specialidad.activo} onChange={handleInputChange} />
                <label className="form-check-label" name="activo" htmlFor="exampleCheck1">Activo</label>
            </div>
            <div className="mb-3">
                <label htmlFor="nombre" className="form-label"> nombre </label>
                <input type="text" className="form-control" name="nombre" value={specialidad.nombre} onChange={handleInputChange} />
            </div>
            <div className="mb-3">
                <label htmlFor="descripcion" className="form-label">descripcion</label>
                <input type="text" className="form-control" name="descripcion" value={specialidad.descripcion} onChange={handleInputChange} />
            </div>
            <div >
                {
                    params.id ? (
                        <button type="submit" className="btn btn-primary">editar</button>
                    ) : (
                        <button type="submit" className="btn btn-primary">Submit</button>
                    )
                }

            </div>


        </form>
    )
};

export default EspecialidadForm;