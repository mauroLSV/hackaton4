import axios from 'axios'
const API_URL = 'http://127.0.0.1:8000/api/pecialidades/';

export const listEspecialidades = async () => {
    return await fetch(API_URL);
};
export const getEspecialidades = async (especialidadId) => {
    return await fetch(`${API_URL}${especialidadId
        }`);
};
export const registerEspecialidades = async (especialidad) => {
    axios.post(API_URL, especialidad, {
        headers: {
            'Content-Type': 'application/json'
        },
    }).then((rest) => { console.log(rest); }).catch((err) => { console.log(err); })
};

export const delteEspecialidades = async (especialidadid) => {
    axios.delete(`${API_URL}${especialidadid}`, {
        headers: {
            'Content-Type': 'application/json'
        },
    }).then((rest) => { console.log(rest); }).catch((err) => { console.log(err); })
};

export const updateEspecialidad = async (especialidadId, updatedEspecialidad) => {

    return axios.put(`${API_URL}${especialidadId}/`, updatedEspecialidad, {
        headers: {
            'Content-Type': 'application/json'
        },

    });
};
