import "bootstrap/dist/css/bootstrap.min.css";

import { BrowserRouter, Route, Routes } from "react-router-dom";

import EspecialidadForm from "./components/especialidades/EspecialidadForm.jsx"
import ListEspecialidades from './components/especialidades/ListEspecialidades'
import MedicoForm from './components/medicos/MedicoForm'
import MedicoList  from "./components/medicos/MedicoList";
import Navbar from './components/Navbar/Navbar'
import React from 'react'
import ReactDOM from 'react-dom/client'

ReactDOM.createRoot(document.getElementById('root')).render(
  <BrowserRouter>
    <Navbar />
    <div className="container my-4">
      <Routes>
        <Route path="/updatemedico/:id" element={<MedicoForm />} />
        <Route path="/medicolist" element={<MedicoList />} />
        <Route path="/medicoform" element={<MedicoForm />} />
        <Route path="/especialidadList" element={<ListEspecialidades />} />
        <Route path="/especialidadForm" element={<EspecialidadForm />} />
        <Route path="/updateespecialidad/:id" element={<EspecialidadForm />} />
      </Routes>
      <React.StrictMode>
      </React.StrictMode>
    </div>
  </BrowserRouter>,

);
